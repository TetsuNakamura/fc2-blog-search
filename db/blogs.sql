CREATE TABLE `blogs` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_name` VARCHAR(255) NOT NULL,
  `server_no` INT UNSIGNED DEFAULT NULL,
  `entry_no` INT UNSIGNED DEFAULT NULL,
  `title` TEXT DEFAULT NULL,
  `description` TEXT DEFAULT NULL,
  `link` VARCHAR(2083) NOT NULL,
  `posted_at` DATETIME NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT 0,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_blogs` (`user_name`,`server_no`,`entry_no`),
  INDEX `idx_server_no`(`server_no`),
  INDEX `idx_Entry_no`(`entry_no`),
  INDEX `idx_posted_at`(`posted_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
