# サーバーの設置方法

## デプロイ(centos7)
conohaサーバーでLAMPパッケージにてサーバーが起動していること

## public directory
/var/www/html


## git
sudo git clone git@bitbucket.org:TetsuNakamura/fc2-blog-search.git  
curl -sS https://getcomposer.org/installer | php  

## 必要なもの
sudo mv composer.phar /usr/bin/composer  
composer install  
sudo yum install epel-release  
sudo yum install nodejs npm  
sudo yum install -y epel-release gcc gcc-c++ git libtool-ll libtool-ltdl-devel lsof make unixODBC unixODBC-devel wget  
sudo yum update openssl  
sudo npm install -g node-gyp bower  
bower install  


## mysql

```
mysql -uroot -p
```

```
CREATE DATABASE exam0115;
grant select, insert, update, delete on exam0115.* to exam0115@'localhost' identified by '68vWvk6CgUJocjL8';

CREATE TABLE `blogs` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_name` VARCHAR(255) NOT NULL,
  `server_no` INT UNSIGNED DEFAULT NULL,
  `entry_no` INT UNSIGNED DEFAULT NULL,
  `title` TEXT DEFAULT NULL,
  `description` TEXT DEFAULT NULL,
  `link` VARCHAR(2083) NOT NULL,
  `posted_at` DATETIME NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT 0,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_blogs` (`user_name`,`server_no`,`entry_no`),
  INDEX `idx_server_no`(`server_no`),
  INDEX `idx_Entry_no`(`entry_no`),
  INDEX `idx_posted_at`(`posted_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```

## crontabを設置する

```
* 5 * * * /usr/bin/php /var/www/html/fc2-blog-search/cli/getNewEntryFromFc2.php
0 0 * * * /usr/bin/php /var/www/html/fc2-blog-search/cli/delete2weeksbefireEntry.php
```
