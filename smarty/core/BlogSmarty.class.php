<?php

require_once '/var/www/html/fc2-blog-search/vendor/autoload.php';

class BlogSmarty extends Smarty {

    private $_db;

    /* set database settings here! */
    // PDO database type
    private $dbtype = 'mysql';
    // PDO database name
    private $dbname = 'exam0115';
    // PDO database host
    private $dbhost = 'localhost';
    // PDO database username
    private $dbuser = 'exam0115';
    // PDO database password
    private $dbpass = '68vWvk6CgUJocjL8';

    private $expire = null;

    public function __construct()
    {
        parent::__construct();
        $this->template_dir = './smarty/templates';
        $this->compile_dir = './smarty/templates_c';
        $this->config_dir = './smarty/configs';
        $this->cache_dir = './smarty/cache';
        $this->default_modifiers = array('escape:"htmlall"');
        $this->expire = time() + 60 * 60 * 24 * 14; // 2weeks有効

        $this->assign('app_title', 'Fc2 Blog Search application');
        //TODO:デプロイ時、本番のDBに変更する
        try {
            $this->_db = new PDO(
                "{$this->dbtype}:dbname={$this->dbname};host={$this->dbhost};charset=utf8",
                $this->dbuser, $this->dbpass
            );
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    /*
     * _dbプロパティ getter
     *
     * @return $this->_db object
     */
    public function getDb() {
        return $this->_db;
    }

    /**
     * 検索フォームからのエントリー出力
     *
     * @param $post array
     * @return $rows array
     *
     */
    public function searchEntries($get) {
        try {
            $this->getDb()->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->getDb()->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

            //cookie保存
            $this->set_cookie_search($get);
            // 検索条件によって組み立てる
            $search_query = $this->build_search_query($get, "search");
            // prepare query
            $stmt = $this->getDb()->prepare($search_query);
            // 検索条件によってバインドする
            $stmt = $this->bind_search_query($stmt, $get, "search");
            // executeでクエリを実行
            $stmt->execute();
            // fetchAllで結果を全件配列で取得
            $rows = $stmt->fetchAll();


        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
            return false;
        }
        return $rows;
    }


    public function getPages($get)
    {
        try {
            $this->getDb()->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->getDb()->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

            $total_query = $this->build_search_query($get, "total");

            // 総件数カウント用SQLを、プリペアドステートメントで実行
            $totalStmt = $this->getDb()->prepare($total_query);
            $totalStmt = $this->bind_search_query($totalStmt, $get, "total");
            $totalStmt->execute();
            $total = $totalStmt->fetch(PDO::FETCH_ASSOC);
            // 総件数÷1ページに表示する件数 を切り上げたものが総ページ数
            $pages["cnt"] = ceil($total["cnt"] / 10);
            $pages["current"] = !empty($get["page"]) ? $get["page"] : 1;
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
            return false;
        }
        return $pages;
    }


    /*
    * クッキーの保存と削除
    * @param $post array
    */
    public function set_cookie_search($get)
    {
        setcookie('posted_at_start', $get["posted_at_start"], $this->expire);
        setcookie('posted_at_end', $get["posted_at_end"], $this->expire);
        setcookie('user_name', $get["user_name"], $this->expire);
        setcookie('server_no', $get["server_no"], $this->expire);
        setcookie('entry_no', $get["entry_no"], $this->expire);
        setcookie('link', $get["link"], $this->expire);
    }

    /*
     * 検索クエリを組み立てる
     *
     * @param $post array
     * @return $query string
     */
    private function build_search_query($get, $type) {
        // クエリを組み立てる
        if($type === "search"){
            $query = "SELECT * FROM `blogs` WHERE 1";
        }
        if($type === "total"){
            $query = "SELECT COUNT(*) as 'cnt' FROM `blogs` WHERE 1";
        }

        if(!empty($get["posted_at_start"])){
            $query .= " AND DATE_FORMAT(posted_at, '%Y-%m-%d') >= :posted_at_start";
        }
        if(!empty($get["posted_at_end"])){
            $query .= " AND DATE_FORMAT(posted_at, '%Y-%m-%d') <= :posted_at_end";
        }
        if(!empty($get["user_name"])){
            $query .= " AND `user_name` LIKE :user_name";
        }
        if(!empty($get["server_no"])){
            $query .= " AND `server_no` = :server_no";
        }
        if(!empty($get["entry_no"])){
            $query .= " AND `entry_no` >= :entry_no";
        }
        if(!empty($get["link"])){
            $query .= " AND `user_name` LIKE :link";
        }
        if($type === "search"){
            // 投稿日 降順
            $query .= " ORDER BY `posted_at` DESC";
            $query .= " LIMIT :start, 10";
        }

        return $query;
    }


    /*
     * 検索の値をbindする
     *
     * @param $stmt PDOStatement
     * @param $post string
     * @return $stmt PDOStatement
     */
    private function bind_search_query($stmt, $get, $type) {

        // 値があればbindする
        if(!empty($get["posted_at_start"])){
            $stmt->bindValue(':posted_at_start', $get["posted_at_start"]);
        }
        if(!empty($get["posted_at_end"])){
            $stmt->bindValue(':posted_at_end', $get["posted_at_end"]);
        }
        if(!empty($get["user_name"])){
            $stmt->bindValue(':user_name', '%' . $get["user_name"] . '%');
        }
        if(!empty($get["server_no"])){
            $stmt->bindValue(':server_no', $get["server_no"]);
        }
        if(!empty($get["entry_no"])){
            $stmt->bindValue(':entry_no', $get["entry_no"]);
        }
        if(!empty($get["link"])){
            $stmt->bindValue(':link', '%' . $get["link"] . '%');
        }

        if($type === "search"){
            $stmt->bindValue(':start', $get["page"] * 10);
        }
        return $stmt;
    }

    /*
     * エントリーが既に存在しているかどうかでinsertとupdateを振り分ける
     * @param data array
     */
    public function insert_entry_data($data) {
        foreach ($data as $entry) {
            if($this->entry_exist($entry->get_link())){
                $this->update_entry($entry);
            }else{
                $this->register_entry($entry);
            }
        }
    }


    /*
     * エントリーURLが存在しなければInsert into
     *
     * @param $entry SimplePie
     * @return $id integer
     */
    private function register_entry($entry){
        $this->getDb()->beginTransaction();
        try {
            $stmt = $this->getDb()->prepare(implode(' ', array(
                'INSERT',
                'INTO blogs(user_name, server_no, entry_no, title, description, link, posted_at)',
                'VALUES (?, ?, ?, ?, ?, ?, ?)',
            )));
            // http://{{username}}.blog{{server_no}}.fc2.com/blog-entry-{{entry_no}}.html
            // {{username}}.blog{{server_no}}.fc2.comを抜き出す
            $host = parse_url($entry->get_permalink(), PHP_URL_HOST);
            // blog-entry-{{entry_no}}を抜き出す
            $path = parse_url($entry->get_permalink(), PHP_URL_PATH);
            // db登録のため値をセット
            $stmt->execute([
                $this->pick_user_name($host),
                $this->pick_server_no($host),
                $this->pick_entry_no($path),
                $entry->get_title(),
                $entry->get_description(),
                $entry->get_permalink(),
                $entry->get_date("Y/m/d H:i:s")
            ]);
            $id = $this->getDb()->lastInsertId();
            $this->getDb()->commit();
            return $id;
        } catch (Exception $e) {
            $this->getDb()->rollBack();
            throw $e;
        }
    }

    /*
     * すでにエントリーURLが存在して入ればupdate
     *
     * @param $entry SimplePie
     * @return $id integer
     */
    private function update_entry($entry)
    {
        $this->getDb()->beginTransaction();
        try {
            $stmt = $this->getDb()->prepare(implode(' ', array(
                'UPDATE',
                'blogs SET user_name=?, server_no=?, entry_no=?, title=?, description=?, link=?, posted_at=?, updated_at=NOW()',
                'WHERE link = ?'
            )));
            // http://{{username}}.blog{{server_no}}.fc2.com/blog-entry-{{entry_no}}.html
            // {{username}}.blog{{server_no}}.fc2.comを抜き出す
            $host = parse_url($entry->get_permalink(), PHP_URL_HOST);
            // blog-entry-{{entry_no}}を抜き出す
            $path = parse_url($entry->get_permalink(), PHP_URL_PATH);
            // db登録のため値をセット

            $stmt->execute([
                $this->pick_user_name($host),
                $this->pick_server_no($host),
                $this->pick_entry_no($path),
                $entry->get_title(),
                $entry->get_description(),
                $entry->get_permalink(),
                $entry->get_date("Y/m/d H:i:s"),
                $entry->get_permalink()
            ]);
            $id = $this->getDb()->lastInsertId();
            $this->getDb()->commit();
            return $id;
        } catch (Exception $e) {
            $this->getDb()->rollBack();
            throw $e;
        }
    }

    public function delete_2weeks_before_entry_data(){
        $this->getDb()->beginTransaction();
        try {
            $stmt = $this->getDb()->prepare(implode(' ', array(
                'DELETE',
                'blogs',
                'WHERE posted_at <= DATE(NOW() - INTERVAL 2 WEEK);'
            )));
            $stmt->execute();
            return $this->getDb()->commit();
        } catch (Exception $e) {
            $this->getDb()->rollBack();
            throw $e;
        }
    }

    /*
     * entry_noを返す
     * @param $path string
     * @return string
     */
    private function pick_entry_no($path){
        return preg_replace('/[^0-9]/', '', $path);
    }

    /*
     * user_nameが存在すればusernameを返す、しなければnullを返す
     *
     * @param $host string
     * @return $user_name|null string|boolean
     */
    private function pick_user_name($host){
        if(!empty($user_name = explode(".", $host,3)[0])){
            return $user_name;
        }
        return null;
    }

    /*
     * server_noが存在すればserver_noを返す、しなければnullを返す
     *
     * @param $link string
     * @return $server_no|null string|boolean
     */
    private function pick_server_no($host){
        $string = explode(".", $host,3);
        if(!empty($string[1])){
            if(strpos($string[1], 'blog') !== false){
                if(($server_no = str_replace('blog', '', $string[1])) !== ""){
                    return $server_no;
                }
            }
        }
        return null;
    }

    /*
     * エントリーがDBに存在するかチェックする
     *
     * @param $link string
     * @return boolean
     */
    public function entry_exist($link) {
        $this->getDb()->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->getDb()->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

        $query = "SELECT link FROM blogs WHERE link = :link LIMIT 1";
        $stmt = $this->getDb()->prepare($query);
        $stmt->bindValue(':link', $link);

        $stmt->execute();
        return (bool)$stmt->fetch();
    }


    /*
     * 引数なしでテンプレに渡せる
     */
    public function d() {
        parent::display(basename($_SERVER['PHP_SELF'], '.php').'.tpl');
    }

}