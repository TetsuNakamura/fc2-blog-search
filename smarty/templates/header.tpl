<html lang="jp">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title> {$app_title} </title>
<script type="text/javascript" src="./lib/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="./lib/moment/min/moment.min.js"></script>
<script type="text/javascript" src="./lib/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="./lib/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" href="./lib/bootstrap/dist/css/bootstrap.min.css"/>
<link rel="stylesheet" href="./lib/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />

<body>
<div class="container">