{* ヘッダー *}
{include file='header.tpl' page_title={$s.app_title}}

<h1>{$app_title}</h1>

<div class="row">

{* 検索フォーム *}
{* 検索条件
日付
URL
ユーザー名
サーバー番号
エントリーNo.
*}
<div id="searchForm">
    {include file='search.tpl' params=$param}
</div>
    {* ページネーション *}
    {if $pages.cnt > 10}
        {include file='page.tpl' pages=$pages}
    {/if}
    {* エントリー表 *}
    <div id="entryTable">
    <table class="table table-striped table-bordered">
      <thead>
      <tr>
        <th>投稿日</th>
        <th>記事URL</th>
        <th>タイトル</th>
        <th>詳細</th>
      </tr>
      </thead>
      <tbody>
        {foreach from=$data item="entry"}
          <tr>
            <td class="col-xs-2">{$entry.posted_at|date_format:"%Y/%m/%d"}</td>
            <td class="col-xs-2">{$entry.link|escape}</td>
            <td class="col-xs-2">{$entry.title|escape}</td>
            <td class="col-xs-6">{$entry.description|escape}</td>
              {foreachelse}
          <tr>
            <td colspan="4">No records</td>
          </tr>
        {/foreach}
      </tbody>
    </table>
  </div>
</div>
{* ヘッダー *}
{include file='footer.tpl'}

<script type="text/javascript">
    $(function () {
        $('#posted_at_start').datetimepicker({
            allowInputToggle: true,
            format: "YYYY-MM-DD"
        });
        $('#posted_at_end').datetimepicker({
            allowInputToggle: true,
            format: "YYYY-MM-DD"
        });
        // ページングクリックイベント
        $('.page-link').on("click", function (e) {
            getParameter(e.target.dataset.page);
            //ページングのパラメータを渡して飛ばす
            window.location.href = getParameter(e.target.dataset.page);
        });
        // ページング用URLパラメータを取得する
        function getParameter(page) {
            var path = location.pathname;
            var param = location.search;
            console.log(param);
            if (path.match(/index.php/) === null) {
                path = path + 'index.php?'
            }
            param = param.substring(0, param.indexOf('page='));
            console.log(param);
            pathParam = path + param + "page=" + page;
            console.log(pathParam);
            return pathParam;
        }
    });
</script>
<style>
    .page{
        text-align: center;
    }
</style>