{* Search *}

<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading" >Fc2新着ブログ検索</div>
            <div class="panel-body">
                {if $error ne ""}
                    <p>
                        {if $error eq "name_empty"}You must supply a name.
                        {elseif $error eq "comment_empty"} You must supply a comment.
                        {/if}
                    </p>
                {/if}
                <form action="{$SCRIPT_NAME}" method="get" class="row form-horizontal"
                      accept-charset="UTF-8" id="searchForm">
                    <div class='form-group col-xs-12 col-md-6'>
                        <label class="col-xs-12 col-md-4 control-label" for="posted_at_start">
                            投稿日
                        </label>
                        <div class="col-xs-12 col-md-8">
                            <div class='input-group date' id='posted_at_start'>
                                <span class="input-group-addon">開始日</span>
                                <input type="text" name="posted_at_start" value="{$params.posted_at_start|escape}"
                                       class="form-control" title="posted_at_start">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class='form-group col-xs-12 col-md-6'>
                        <label class="col-xs-12 col-md-4 control-label" for="posted_at_end">
                            投稿日
                        </label>
                        <div class="col-xs-12 col-md-8">
                            <div class='input-group date' id='posted_at_end'>
                                <span class="input-group-addon">終了日</span>
                                <input type="text" name="posted_at_end" value="{$params.posted_at_end|escape}"
                                       class="form-control" title="posted_at_end">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class='form-group col-xs-12 col-md-6'>
                        <label class="col-xs-12 col-md-4 control-label" for="link">
                            URL
                        </label>
                        <div class="col-xs-12 col-md-8">
                            <input type="text" name="link" value="{$params.link|escape}" class="form-control"
                                   title="link">
                        </div>
                    </div>
                    <div class='form-group col-xs-12 col-md-6'>
                        <label class="col-xs-12 col-md-4 control-label" for="user_name">
                            ユーザー名
                        </label>
                        <div class="col-xs-12 col-md-8">
                            <input type="text" name="user_name" value="{$params.user_name|escape}" class="form-control"
                                   title="user_name">
                        </div>
                    </div>
                    <div class='form-group col-xs-12 col-md-6'>
                        <label class="col-xs-12 col-md-4 control-label" for="server_no">
                            サーバー番号
                        </label>
                        <div class="col-xs-12 col-md-8">
                            <input type="text" name="server_no" value="{$params.server_no|escape}" class="form-control"
                                   title="server_no">
                        </div>
                    </div>
                    <div class='form-group col-xs-12 col-md-6'>
                        <label class="col-xs-12 col-md-4 control-label" for="entry_no">
                            エントリーNo.
                        </label>
                        <div class="col-xs-12 col-md-8">
                            <div class="input-group">
                                <input type="text" name="entry_no" value="{$params.entry_no|escape}" class="form-control"
                                       title="entry_no">
                                <span class="input-group-addon">入力値以上</span>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="page" value="" id="page">
                    <div class='form-group col-xs-12'>
                        <div class='col-xs-12 col-md-5 col-md-offset-2'>
                            <input class="btn btn-primary btn-search" type="submit" value="検索">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
