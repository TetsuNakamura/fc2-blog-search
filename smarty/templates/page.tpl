<nav aria-label="pager" class="page">
    <ul class="pagination">
        <li class="page-item disabled">
            {if $pages.current !== 1}
                <a class="page-link" tabindex="-1" data-page="{$pages.current - 1}">前へ</a>
            {/if}
        </li>
        {section name=page start=1 loop=$pages.cnt step=1}
            {if (($pages.current - $smarty.section.page.index) < 8
            and ($pages.current - $smarty.section.page.index) >= 0)
            or
            (($smarty.section.page.index - $pages.current) < 8)
            and (($smarty.section.page.index - $pages.current) >= 0)
            }
                {if $pages.current == $smarty.section.page.index}
                    <li class="page-item active"><a>{$smarty.section.page.index}<span class="sr-only">(current)</span></a></li>
                {else}
                    <li class="page-item"><a class="page-link" data-page="{$smarty.section.page.index}">{$smarty.section.page.index}</a></li>
                {/if}
            {/if}
        {/section}
        <li class="page-item">
            <a class="page-link" data-page="{$pages.current + 1}">次へ</a>
        </li>
    </ul>
</nav>
