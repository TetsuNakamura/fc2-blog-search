<?php
require_once '/var/www/html/fc2-blog-search/vendor/autoload.php';
require_once '/var/www/html/fc2-blog-search/smarty/core/BlogSmarty.class.php';

ini_set('date.timezone', 'Asia/Tokyo');

$url = 'http://blog.fc2.com/newentry.rdf';

$feed = new SimplePie();
$feed->set_feed_url($url);
$feed->enable_cache(false); //キャッシュ機能はオフで使う
$success = $feed->init();
$feed->handle_content_type();

if ($success){
    $s = new BlogSmarty();
    $s->insert_entry_data($feed->get_items());

}else{
    echo $feed->error();
}
