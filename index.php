<?php
require_once 'vendor/autoload.php';
require_once 'smarty/core/BlogSmarty.class.php';

ini_set('date.timezone', 'Asia/Tokyo');

$s = new BlogSmarty();

// set the current action
$action = $_SERVER["QUERY_STRING"] === "" ? 'index' : 'search';
switch($action) {
    case 'search':
        $s->set_cookie_search($_GET);
        $param = $_GET;
        break;
    default:
        $param = $_COOKIE;
        $param["current"] = 1;
}

$entry = $s->searchEntries($param);
$pages = $s->getPages($param);
$s->assign('param', $param);
$s->assign('data', $entry);
$s->assign('pages', $pages);

$s->d();
